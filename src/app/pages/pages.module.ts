import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PAGES_ROUTE } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientesComponent } from './clientes/clientes.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ConceptopuntoComponent } from './conceptopunto/conceptopunto.component';
import { BolsapuntoComponent } from './bolsapunto/bolsapunto.component';
import { UsopuntoComponent } from './usopunto/usopunto.component';

@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,
    ClientesComponent,
    ConceptopuntoComponent,
    BolsapuntoComponent,
    UsopuntoComponent
   
  ],
  exports: [
    PagesComponent,
    DashboardComponent,
    ConceptopuntoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PAGES_ROUTE,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PagesModule { }
