import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/service.index';
import { Cliente } from 'src/app/models/Cliente';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[] = [];
  constructor(public _clienteService: ClienteService,
   ) {

  }

  ngOnInit() {
    this.cargarCliente();
  }

  cargarCliente() {
    this._clienteService.cargarUsuarios().subscribe((resp: Cliente[]) => {
      this.clientes=resp;
      console.log(resp);
    });
  }

}
