
import { Routes, RouterModule } from '@angular/router';

import { PagesComponent, } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ConceptopuntoComponent } from './conceptopunto/conceptopunto.component';
import { BolsapuntoComponent } from './bolsapunto/bolsapunto.component';
import { UsopuntoComponent } from './usopunto/usopunto.component';

const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent, data: {titulo: 'Dashboard'}},
      {path: 'cliente', component: ClientesComponent, data: {titulo: 'Cliente'}},
      {path: 'concepto', component: ConceptopuntoComponent, data: {titulo: 'Concepto Punto'}},
      {path: 'bolsa', component: BolsapuntoComponent, data: {titulo: 'Bolsa Punto'}},
      {path: 'usopunto', component:UsopuntoComponent,data:{titulo:'Uso Punto'}},
      {path: '', redirectTo: '/', pathMatch: 'full'},
    ]
  }];
export const PAGES_ROUTE = RouterModule.forChild(pagesRoutes);


