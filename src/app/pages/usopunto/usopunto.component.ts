import { Cliente } from './../../models/Cliente';
import { ConceptoPunto } from './../../models/ConceptoPunto';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ViewChild, ElementRef} from '@angular/core';
import { UsoPunto } from 'src/app/models/UsoPunto';
import { UsopuntoService } from 'src/app/services/usopunto/usopunto.service';
import { isUndefined } from 'util';
declare var $: any;

@Component({
  selector: 'app-usopunto',
  templateUrl: './usopunto.component.html',
  styleUrls: ['./usopunto.component.css']
})
export class UsopuntoComponent implements OnInit {
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  FormCP:FormGroup;
  listusopuntos: UsoPunto[] = [];
  listconcpuntos:ConceptoPunto[]=[];
  clientes: Cliente[] = [];
  private clienteSelected:string;
  private conceptoSelected:string;
  private clienteSelected2:string;
  private conceptoSelected2:string;
  private fechaDesde:string;
  private fechaHasta:string;
  private utilizarPuntosDialog:string;
  private display='none';
  constructor(public _usopuntoService: UsopuntoService) { }

  ngOnInit() {
    this.cargarUsoPuntos();
    this.cargarCP();
    this.cargarCliente();
  }
  abrirModal(){
    this.utilizarPuntosDialog=""
    this.display='block';
  }
  cerrar(){
    this.display='none';
  }
  cerrarModal(){
    this.display='none';
    this._usopuntoService.postUtilizarPuntos(this.clienteSelected2,this.conceptoSelected2);
    //mientras se arregla lo de Promise
    console.log("busqueda de vuelta");
    this.buscar();
    this.buscar();

  }
  utilizarPuntos(){
    this.abrirModal();
  
  }
  buscar(){
    if(isUndefined(this.conceptoSelected) || this.conceptoSelected=="Todos"){
      console.log("Es undefined");
      this.conceptoSelected="todos";      
    }  
    if(isUndefined(this.clienteSelected) || this.clienteSelected=="Todos"){
      console.log("Es undefined");
      this.clienteSelected="todos";      
    }  
    if(isUndefined(this.fechaDesde) || this.fechaDesde=="2019-05-17"){
      console.log("Es undefined");
      this.fechaDesde="2019-05-17";
    }
    if(this.fechaDesde==""){
      this.fechaDesde="todos";
    }
    if(isUndefined(this.fechaHasta) || this.fechaHasta=="2019-05-17"){
      console.log("Es undefined");
      this.fechaHasta="2019-05-17";
    }
    if(this.fechaHasta==""){
      this.fechaHasta="todos";
    }

    console.log(this.conceptoSelected);
    console.log(this.clienteSelected);
    console.log(this.fechaDesde);
    console.log(this.fechaHasta);
    this._usopuntoService.cargarUsoPuntoEspecifico(this.conceptoSelected,this.clienteSelected,this.fechaDesde,this.fechaHasta).subscribe((resp: UsoPunto[]) => {
      this.listusopuntos=resp;
      console.log(resp);
    });
  }
  cambioCliente(event:any){
    console.log(event.target.value);
    this.clienteSelected=event.target.value;
  }
  cambioConcepto(event:any){
    console.log(event.target.value);
    this.conceptoSelected=event.target.value;
  }
  cambioCliente2(event:any){
    console.log(event.target.value);
    this.clienteSelected2=event.target.value;
  }
  cambioConcepto2(event:any){
    console.log(event.target.value);
    this.conceptoSelected2=event.target.value;
  }

  cargarUsoPuntos() {
    this._usopuntoService.cargarUsoPunto().subscribe((resp: UsoPunto[]) => {
      this.listusopuntos=resp;
      console.log(resp);
    });
  }

  cargarCP() {
    this._usopuntoService.cargarConceptoPunto().subscribe((cps: ConceptoPunto [] ) => {
      this.listconcpuntos = cps;
    });


  }

  cargarCliente() {
    this._usopuntoService.cargarUsuarios().subscribe((resp: Cliente[]) => {
      this.clientes=resp;
      console.log(resp);
    });
  }
}
