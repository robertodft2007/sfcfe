import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsopuntoComponent } from './usopunto.component';

describe('UsopuntoComponent', () => {
  let component: UsopuntoComponent;
  let fixture: ComponentFixture<UsopuntoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsopuntoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsopuntoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
