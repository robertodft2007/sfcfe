import { Component, OnInit } from '@angular/core';
import { GestionpuntosService } from 'src/app/services/gestionpuntos/gestionpuntos.service';
import { ConceptoPunto } from 'src/app/models/ConceptoPunto';
import { Cliente } from 'src/app/models/Cliente';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ViewChild, ElementRef} from '@angular/core';
import { BolsaPunto } from 'src/app/models/BolsaPunto';
import { isUndefined } from 'util';
declare var $: any;



@Component({
  selector: 'app-conceptopunto',
  templateUrl:'./bolsapunto.component.html',
  styleUrls: ['./bolsapunto.component.css']
})


export class BolsapuntoComponent implements OnInit {
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  FormCP:FormGroup;
  listbolsapuntos: BolsaPunto[] = [];
  private clientes:any
  private clienteSelected:string;
  private estadoSelected:string;
  private vencimiento:string;
  constructor(public _gestionpuntoService: GestionpuntosService) {}


  ngOnInit() {
    this.FormCP=new FormGroup({
      descpunto:new FormControl(''),
      puntoreq:new FormControl(1)
    });

    this.cargarBP();
    this.cargarCliente();

  }
  cargarBP() {
    this._gestionpuntoService.cargarBolsaPunto().subscribe((bps: BolsaPunto [] ) => {
      this.listbolsapuntos = bps;
    });



  }
  buscar() {
    if(isUndefined(this.clienteSelected) || this.clienteSelected=="Todos"){
      console.log("Es undefined");
      this.clienteSelected="todos";      
    }  
    if(isUndefined(this.estadoSelected) || this.estadoSelected=="Todos"){
      console.log("Es undefined");
      this.estadoSelected="todos";      
    }  
    if(isUndefined(this.vencimiento) || this.vencimiento==""){
      console.log("Es undefined");
      this.vencimiento="todos"; 
    }
    console.log("cliente: "+this.clienteSelected+" estado: "+this.estadoSelected+" vencimiento: "+this.vencimiento);
    this._gestionpuntoService.recargarBolsaPunto(this.clienteSelected,this.estadoSelected,this.vencimiento).subscribe((bps: BolsaPunto [] ) => {
      this.listbolsapuntos = bps;
    });
  }
  guardarCP() {
    let cop= new BolsaPunto(
      this.FormCP.value.descpunto,
      this.FormCP.value.puntoreq,

    );
    /*this._gestionpuntoService.guardarConceptoPunto(cop).subscribe(resp=>{
      console.log("hey jude");
     // this.cargarCP();
      this.closeAddExpenseModal.nativeElement.click();
      return;
    });*/
  }
  cambioCliente(event:any){
    console.log(event.target.value);
    this.clienteSelected=event.target.value;
  }
  cambioEstado(event:any){
    console.log(event.target.value);
    this.estadoSelected=event.target.value;
  }

  cargarCliente() {
    this._gestionpuntoService.cargarUsuarios().subscribe((resp: Cliente[]) => {
      this.clientes=resp;
      console.log(resp);
    });
    
  }


}

