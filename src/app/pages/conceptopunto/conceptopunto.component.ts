import { Component, OnInit } from '@angular/core';
import { GestionpuntosService } from 'src/app/services/gestionpuntos/gestionpuntos.service';
import { ConceptoPunto } from 'src/app/models/ConceptoPunto';
import { Cliente } from 'src/app/models/Cliente';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ViewChild, ElementRef} from '@angular/core';




@Component({
  selector: 'app-conceptopunto',
  templateUrl: './conceptopunto.component.html',
  styleUrls: ['./conceptopunto.component.css']
})


export class ConceptopuntoComponent implements OnInit {
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  FormCP:FormGroup;
  listconcpuntos: ConceptoPunto[] = [];
  constructor(public _gestionpuntoService: GestionpuntosService) {}


  ngOnInit() {
    this.FormCP=new FormGroup({
      descpunto:new FormControl(''),
      puntoreq:new FormControl(1)
    });

    this.cargarCP();


  }
  cargarCP() {
    this._gestionpuntoService.cargarConceptoPunto().subscribe((cps: ConceptoPunto [] ) => {
      this.listconcpuntos = cps;
    });


  }
  guardarCP() {
    let cop= new ConceptoPunto(
      this.FormCP.value.descpunto,
      this.FormCP.value.puntoreq,

    );
    this._gestionpuntoService.guardarConceptoPunto(cop).subscribe(resp=>{
      console.log("hey jude");
      this.cargarCP();
      this.closeAddExpenseModal.nativeElement.click();
      return;
    });
  }


}

