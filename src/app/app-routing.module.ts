import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';


const appRoutes: Routes = [
  {path: '**', component: AppComponent}
];
export const APP_ROUTE = RouterModule.forRoot(appRoutes, {useHash: true})

