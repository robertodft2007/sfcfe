import { ConceptoPunto } from './ConceptoPunto';
import { Cliente } from './Cliente';

export class UsoPunto{
    constructor(
        public cliente: Cliente,
        public concepto_punto:ConceptoPunto,
        public punto_utilizado?: number,
        public fecha_asignado?: string,
        public id?: number
    ){

    }
   

}