import { Cliente } from './Cliente';

export class BolsaPunto{
    constructor(
        public cliente: Cliente,
        public monto_operacion: number,
        public punto_asignado?: number,
        public punto_utilizado?: number,
        public saldo_punto?:number,
        public fecha_asignado?: string,
        public fecha_caducidad?: string,
        public id?: number

    ){

    }
   

}