export class Cliente{
    constructor(
        public nombre:string,
        public apellido:string,
        public ci: number,
        public email: string,
        public telefono: number,
        public fechanaciento: string,
        public role: string,
        public id?: number

    ){

    }
   

}