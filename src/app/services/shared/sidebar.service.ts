import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  menu: any = [
    {
      titulo: ' Clientes',
      icono: 'fas fa-fw  fa-users',
    }, {
      titulo: ' Gestion de Punto',
      icono: 'fas fa-fw fa-coins',
    }, {
      titulo: ' Bolsa Punto',
      icono: 'fas fa-fw fa-piggy-bank',
    }, {
      titulo: ' Uso Punto',
      icono: 'fas fa-fw fa-gift',
    },
  ];
  constructor() { }
}
