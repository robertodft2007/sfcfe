import { TestBed } from '@angular/core/testing';

import { UsopuntoService } from './usopunto.service';

describe('UsopuntoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsopuntoService = TestBed.get(UsopuntoService);
    expect(service).toBeTruthy();
  });
});
