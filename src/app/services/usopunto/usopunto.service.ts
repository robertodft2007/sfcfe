import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsopuntoService {

  constructor(public http: HttpClient) { }

  cargarUsoPunto() {
    const url = URL_SERVICIOS + '/usopunto'+'/todos/todos/todos/todos';
    console.log(URL_SERVICIOS + '/usopunto'+'/todos/todos/todos/todos');

    return this.http.get(url).pipe(map((resp: any) => {
      return resp.uso_punto;
    }));

  }
  cargarUsoPuntoEspecifico(concepto:string,cliente:string,fechaDesde:string,fechaHasta:string) {
    const url = URL_SERVICIOS + '/usopunto'+"/"+concepto+"/"+fechaDesde+"/"+fechaHasta+"/"+cliente;
    console.log(URL_SERVICIOS + '/usopunto'+"/"+concepto+"/"+fechaDesde+"/"+fechaHasta+"/"+cliente);

    return this.http.get(url).pipe(map((resp: any) => {
      return resp.uso_punto;
    }));

  }
  public postUtilizarPuntos(cliente:string,concepto:string){
    console.log(cliente+"llego");
    console.log(concepto+"llego");
    
    var url:string="http://localhost:3000/sfc/usopunto";
      const body = new HttpParams()
        .set('cliente', cliente)
        .set('concepto_punto', concepto);
    
      this.http.post(url,
        body.toString(),
        {
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        }
      ).subscribe(
        (res) => {
            console.log(res);
        },
        err => console.log(err));
    }
  

  cargarConceptoPunto() {
    const url = URL_SERVICIOS + '/punto';

    return this.http.get(url).pipe(map((resp: any) => {
      console.log(resp)
      return resp.puntos;
    }));

  }

  cargarUsuarios() {
    const url = URL_SERVICIOS + '/cliente';

    return this.http.get(url).pipe(map((resp: any) => {
      return resp.clientes;
    }));

  }


}
