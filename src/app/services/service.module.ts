import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {
  SidebarService,
  SharedService,
} from './service.index';




@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    SidebarService,
    SharedService,
    CommonModule,
  //  SubirArchivosService,
  ],
  declarations: []
})
export class ServiceModule { }


