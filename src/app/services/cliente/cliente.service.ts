import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(public http: HttpClient) { }

  cargarUsuarios(desde: number = 0) {
    const url = URL_SERVICIOS + '/cliente';

    return this.http.get(url).pipe(map((resp: any) => {
      return resp.clientes;
    }));

  }
}
