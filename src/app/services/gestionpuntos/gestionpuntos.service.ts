import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators';
import { ConceptoPunto } from 'src/app/models/ConceptoPunto';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GestionpuntosService {

  constructor(public http: HttpClient) { }


  //CONCEPTO PUNTO
  cargarConceptoPunto() {
    const url = URL_SERVICIOS + '/punto';

    return this.http.get(url).pipe(map((resp: any) => {
      console.log(resp)
      return resp.puntos;
    }));

  }
  guardarConceptoPunto(concpunt: ConceptoPunto) {
    const url = URL_SERVICIOS + '/punto';

    return this.http.post(url, concpunt).pipe(map((resp: any) => {
      console.log(resp);
      return resp;
    }));
  }


  //BOLSA PUNTO


  cargarBolsaPunto() {
    const url = URL_SERVICIOS + '/bolsapunto';

    return this.http.get(url).pipe(map((resp: any) => {
      console.log(resp)
      return resp.bolsas;
    }));

  }
  recargarBolsaPunto(cliente:string,estado:string,vencimiento:string) {
    const url = URL_SERVICIOS + '/bolsapunto';
    console.log("cliente: "+cliente+" estado: "+estado+" vencimiento: "+vencimiento);
    return this.http.get(url+"/"+cliente+"/"+estado+"/"+vencimiento).pipe(map((resp: any) => {
      console.log(resp)
      return resp.bolsas;
    }));

  }
  cargarUsuarios(desde: number = 0) {
    const url = URL_SERVICIOS + '/cliente';

    return this.http.get(url).pipe(map((resp: any) => {
      return resp.clientes;
    }));

  }


}


